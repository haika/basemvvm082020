package com.vmo.mvvmbaseapp.helper

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.vmo.mvvmbaseapp.R

const val NOTIFICATION_CHANNEL_ID = "NOTIFICATION_CHANNEL"
const val NOTIFICATION_CHANNEL_NAME = "NOTIFICATION_CHANNEL_NAME"
const val NOTIFICATION_CHANNEL_DESC = "NOTIFICATION_CHANNEL_DESC"

data class NotificationMessage(
    val id: Int,
    val title: String,
    val contentText: String,
    val subText: String,
    val contentInfo: String
)

class NotificationSender {

    companion object {

        @TargetApi(Build.VERSION_CODES.O)
        private fun createChannel(): NotificationChannel {
            return NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                description = NOTIFICATION_CHANNEL_DESC
                setShowBadge(true)
            }
        }

        private fun createNotification(context: Context, message: NotificationMessage): Notification {
            return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSubText(message.subText)
                .setContentText(message.contentText)
                .setContentInfo(message.contentInfo)
                .setContentTitle(message.title)
                .setSmallIcon(R.drawable.notification_icon_background)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build()
        }

        fun sendNotification(context: Context, message: NotificationMessage) {
            val notification = createNotification(context, message)
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel = createChannel()
                notificationManager.createNotificationChannel(notificationChannel)
            }

            notificationManager.notify(message.id, notification)
        }
    }
}