package com.vmo.mvvmbaseapp.helper

import com.vmo.mvvmbaseapp.network.AppModule
import com.vmo.mvvmbaseapp.ui.activity.MainActivity
import com.vmo.mvvmbaseapp.viewmodel.LoginViewModel
import com.vmo.mvvmbaseapp.viewmodel.MovieViewModel
import com.vmo.mvvmbaseapp.worker.SyncMessageWorker
import com.vmo.mvvmbaseapp.worker.SyncItemsWorker
import dagger.Component


@Component(modules = [AppModule::class])
interface MagicBox {
    fun poke(mainActivity: MainActivity)
    fun poke(loginViewModel: LoginViewModel)
    fun poke(movieViewModel: MovieViewModel)
    fun poke(syncMessageWorker: SyncMessageWorker)
    fun poke(syncItemWorker: SyncItemsWorker)
}