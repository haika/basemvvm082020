package com.vmo.mvvmbaseapp.network.api

import com.google.samples.apps.sunflower.data.UnsplashSearchResponse
import com.vmo.mvvmbaseapp.model.Item
import com.vmo.mvvmbaseapp.model.auth.Credential
import com.vmo.mvvmbaseapp.model.auth.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ResApi {
    @POST("token/")
    suspend fun loginUser(@Body credential: Credential): User

    @GET("movies/favorites/")
    suspend fun getItems(): List<Item>

    @GET("search/photos")
    suspend fun searchPhotos(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): UnsplashSearchResponse
}