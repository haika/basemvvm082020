package com.vmo.mvvmbaseapp.network.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vmo.mvvmbaseapp.model.auth.Credential
import com.vmo.mvvmbaseapp.model.auth.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserRepository @Inject constructor() : BaseRepository<User>() {

    fun loginUser(username: String, password: String): LiveData<User?> {
        val liveData = MutableLiveData<User>()
        val job = Job()
        CoroutineScope(Dispatchers.IO + job).launch {
            try {
                val user = resApi.loginUser(Credential(username, password))
                liveData.postValue(user)
            } catch (e: Exception) {
                e.printStackTrace()
                liveData.postValue(null)
            }
        }

        return liveData
    }

    fun logOutUser(): LiveData<Boolean> {
        return MutableLiveData<Boolean>().apply { value = true }
    }

}
