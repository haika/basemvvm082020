package com.vmo.mvvmbaseapp.network

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.vmo.mvvmbaseapp.common.DATABASE_NAME
import com.vmo.mvvmbaseapp.dao.ItemDAO
import com.vmo.mvvmbaseapp.model.Item

@Database(entities = [Item::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieDAO(): ItemDAO

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java, DATABASE_NAME
                ).build()
            }
            return instance
        }

    }

}