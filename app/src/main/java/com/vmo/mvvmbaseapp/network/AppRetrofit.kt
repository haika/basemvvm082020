package com.vmo.mvvmbaseapp.network

import android.content.Context
import com.vmo.mvvmbaseapp.common.BASE_URI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppRetrofit {
    companion object {
        private var instance: Retrofit? = null
        fun getInstance(context: Context): Retrofit? {
            if (instance == null) {
                instance = Retrofit
                    .Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URI)
                    .build()
            }
            return instance
        }

    }
}