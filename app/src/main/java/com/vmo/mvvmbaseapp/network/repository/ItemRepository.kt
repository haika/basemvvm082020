package com.vmo.mvvmbaseapp.network.repository

import androidx.lifecycle.LiveData
import com.vmo.mvvmbaseapp.model.Item
import com.vmo.mvvmbaseapp.model.auth.User
import javax.inject.Inject

class ItemRepository @Inject constructor() : BaseRepository<Item>(), IFetchRoomData<Item>, IFetchApiData<Item> {

    @Inject
    lateinit var user: User

    fun updateAllItems(items: List<Item>) {
        appDatabase.movieDAO().updateAllItems(items)
    }

    override fun fetchDataFromRoom(): LiveData<List<Item>> {
        return appDatabase.movieDAO().selectAll()
    }

    override suspend fun fetchDataFromApi(): List<Item> {
        return resApi.getItems()
    }
}
