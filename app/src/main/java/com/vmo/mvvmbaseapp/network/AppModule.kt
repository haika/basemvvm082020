package com.vmo.mvvmbaseapp.network

import android.content.Context
import com.google.gson.Gson
import com.vmo.mvvmbaseapp.common.MODE_PRIVATE
import com.vmo.mvvmbaseapp.common.USER_KEY
import com.vmo.mvvmbaseapp.common.USER_PREFERENCE
import com.vmo.mvvmbaseapp.model.auth.User
import com.vmo.mvvmbaseapp.network.api.ResApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class AppModule constructor(private val context: Context) {

    @Provides
    fun provideRetrofit(): Retrofit {
        return AppRetrofit.getInstance(context)!!
    }

    @Provides
    fun provideAppDatabase(): AppDatabase {
        return AppDatabase.getInstance(context)!!
    }

    @Provides
    fun provideUser(): User {
        val userJson = context.getSharedPreferences(
            USER_PREFERENCE,
            MODE_PRIVATE
        ).getString(USER_KEY, "{}")
        return Gson().fromJson(userJson, User::class.java)
    }

    @Provides
    fun provideUserService(retrofit: Retrofit): ResApi {
        return retrofit.create(ResApi::class.java)
    }

}