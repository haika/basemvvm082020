package com.vmo.mvvmbaseapp.network.repository

import androidx.lifecycle.LiveData
import com.vmo.mvvmbaseapp.network.AppDatabase
import com.vmo.mvvmbaseapp.network.api.ResApi
import javax.inject.Inject

interface IFetchRoomData<T> {
    fun fetchDataFromRoom(): LiveData<List<T>>
}

interface IFetchApiData<T> {
    suspend fun fetchDataFromApi(): List<T>
}

abstract class BaseRepository<T> {
    @Inject
    protected lateinit var resApi: ResApi

    @Inject
    protected lateinit var appDatabase: AppDatabase

}