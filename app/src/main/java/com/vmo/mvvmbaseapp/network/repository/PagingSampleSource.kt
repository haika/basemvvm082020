package com.vmo.mvvmbaseapp.network.repository

import androidx.paging.PagingSource
import com.google.samples.apps.sunflower.data.UnsplashPhoto
import com.vmo.mvvmbaseapp.network.api.ResApi
private const val UNSPLASH_STARTING_PAGE_INDEX = 1
class PagingSampleSource(
    private val service: ResApi,
    private val query: String
) : PagingSource<Int, UnsplashPhoto>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UnsplashPhoto> {
        val page = params.key ?: UNSPLASH_STARTING_PAGE_INDEX
        return try {
            val response = service.searchPhotos(query, page, params.loadSize)
            val photos = response.results
            LoadResult.Page(
                data = photos,
                prevKey = if (page == UNSPLASH_STARTING_PAGE_INDEX) null else page - 1,
                nextKey = if (page == response.totalPages) null else page + 1
            )
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }
}