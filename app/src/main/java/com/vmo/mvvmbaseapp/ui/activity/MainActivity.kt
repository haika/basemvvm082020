package com.vmo.mvvmbaseapp.ui.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.work.WorkManager
import com.vmo.mvvmbaseapp.R
import com.vmo.mvvmbaseapp.common.authenticate
import com.vmo.mvvmbaseapp.viewmodel.LoginViewModel
import com.vmo.mvvmbaseapp.worker.CustomWorkerFactory
import com.vmo.mvvmbaseapp.worker.SyncItemsWorker
import com.vmo.mvvmbaseapp.worker.SyncMessageWorker


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
//        val itemViewModel = ViewModelProviders.of(this).get(MovieViewModel::class.java)

        loginViewModel
            .loginUser("test_admin", "@xyz3pdld")
            .observe(this, Observer {
                it?.authenticate(this)
            })

//        itemViewModel.getMovies().observe(this, Observer<List<Item>> {
//            val adapter = ItemAdapter(it)
//
//            itemRecyclerView.setHasFixedSize(true)
//            itemRecyclerView.layoutManager = LinearLayoutManager(this)
//            itemRecyclerView.adapter = adapter
//        })

        runSyncItemWork()
        runSyncMessageWork()

        val host = NavHostFragment.create(R.navigation.navigate_main)
        supportFragmentManager.beginTransaction().replace(R.id.container, host)
            .setPrimaryNavigationFragment(host).commit()
    }

    private fun runSyncItemWork() {
        val workerRequest = CustomWorkerFactory.create(SyncItemsWorker::class.java)
        WorkManager.getInstance().enqueue(workerRequest!!)
    }

    private fun runSyncMessageWork() {
        val workerRequest = CustomWorkerFactory.create(SyncMessageWorker::class.java)
        WorkManager.getInstance().enqueue(workerRequest!!)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
