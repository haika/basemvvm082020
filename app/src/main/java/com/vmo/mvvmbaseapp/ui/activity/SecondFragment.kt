package com.vmo.mvvmbaseapp.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vmo.mvvmbaseapp.R
import com.vmo.mvvmbaseapp.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.second_fragment.*

class SecondFragment : Fragment() {

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginViewModel.repository
            .loginUser("aa", "aa")
            .observe(this, Observer {
                showWelcome.text ="DOne"
        })
        return inflater.inflate(R.layout.second_fragment, container, false)
    }
}