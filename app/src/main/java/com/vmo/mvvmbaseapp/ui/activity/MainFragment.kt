package com.vmo.mvvmbaseapp.ui.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.vmo.mvvmbaseapp.databinding.MainFragmentBinding

class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        bundle: Bundle?
    ): View {
        val binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.goToSecondFrg.setOnClickListener {
            val direction = MainFragmentDirections.action1()
            findNavController().navigate(direction)

        }
        return binding.root
    }
}


