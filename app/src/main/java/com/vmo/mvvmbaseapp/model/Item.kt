package com.vmo.mvvmbaseapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Item(@PrimaryKey(autoGenerate = true) val id: Long, val title: String, val year: Int)