package com.vmo.mvvmbaseapp.model.auth

data class User(val id: Long, val username: String)
