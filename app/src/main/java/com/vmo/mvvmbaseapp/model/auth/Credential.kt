package com.vmo.mvvmbaseapp.model.auth

data class Credential(val username: String, val password: String)
