package com.vmo.mvvmbaseapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.vmo.mvvmbaseapp.helper.DaggerMagicBox
import com.vmo.mvvmbaseapp.model.auth.User
import com.vmo.mvvmbaseapp.network.AppModule
import com.vmo.mvvmbaseapp.network.repository.UserRepository
import javax.inject.Inject

class LoginViewModel(app: Application) : AndroidViewModel(app) {
    @Inject
    lateinit var repository: UserRepository

    init {
        DaggerMagicBox.builder().appModule(
            AppModule(
                app
            )
        ).build().poke(this)
    }

    fun loginUser(username: String, password: String): LiveData<User?> {
        return repository.loginUser(username, password)
    }

    fun logOutUser(): LiveData<Boolean> {
        return repository.logOutUser()
    }

}