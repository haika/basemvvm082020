package com.vmo.mvvmbaseapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.vmo.mvvmbaseapp.network.AppModule
import com.vmo.mvvmbaseapp.helper.DaggerMagicBox
import com.vmo.mvvmbaseapp.model.Item
import com.vmo.mvvmbaseapp.network.repository.ItemRepository
import javax.inject.Inject

class MovieViewModel(app: Application) : AndroidViewModel(app) {

    @Inject
    lateinit var repository: ItemRepository

    init {
        DaggerMagicBox.builder().appModule(
            AppModule(
                app
            )
        ).build().poke(this)
    }

    fun getMovies(): LiveData<List<Item>> {
        return repository.fetchDataFromRoom()
    }
}