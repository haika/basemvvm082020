package com.vmo.mvvmbaseapp.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDAO<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: List<T>): List<Long>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(data: T): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(data: List<T>): Int

    @Delete
    fun delete(data: T)

    @Delete
    fun delete(data: List<T>)
}