package com.vmo.mvvmbaseapp.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.vmo.mvvmbaseapp.model.Item

@Dao
abstract class ItemDAO : BaseDAO<Item> {

    @Query("select * from Item")
    abstract fun selectAll(): LiveData<List<Item>>

    @Query("delete from Item")
    abstract fun deleteAll()

    fun updateAllItems(data: List<Item>) {
        deleteAll()
        insert(data)
    }
}