package com.vmo.mvvmbaseapp.common

/**
 * Constants used throughout the app.
 */
const val DATABASE_NAME = "app-db"
const val BASE_URI ="https://favorites-movie.herokuapp.com/api/"