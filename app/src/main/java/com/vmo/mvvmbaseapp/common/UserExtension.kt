package com.vmo.mvvmbaseapp.common

import android.content.Context
import com.google.gson.Gson
import com.vmo.mvvmbaseapp.model.auth.User

const val USER_PREFERENCE = "USER_PREFERENCE"
const val USER_KEY = "USER"
const val MODE_PRIVATE = Context.MODE_PRIVATE

/**
 * Authenticate user locally
 */
fun User.authenticate(context: Context) {
    val share = context.getSharedPreferences(
        USER_PREFERENCE,
        MODE_PRIVATE
    )
    val editor = share.edit()
    editor.putString(USER_KEY, Gson().toJson(this))
    editor.apply()
}

/**
 * DeAuthenticate user locally
 */
fun User.deAuthenticate(context: Context) {
    val editor = context.getSharedPreferences(
        USER_PREFERENCE,
        MODE_PRIVATE
    ).edit()
    editor.clear()
    editor.apply()
}

/**
 * Check if the user is authenticated
 */
fun User.isAuthenticated(context: Context): Boolean {
    val share = context.getSharedPreferences(
        USER_PREFERENCE,
        MODE_PRIVATE
    )
    return share.contains(USER_KEY)
}