package com.vmo.mvvmbaseapp.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.vmo.mvvmbaseapp.network.AppModule
import com.vmo.mvvmbaseapp.helper.DaggerMagicBox
import com.vmo.mvvmbaseapp.network.repository.ItemRepository
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException
import javax.inject.Inject

class SyncItemsWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    @Inject
    lateinit var itemRepository: ItemRepository

    init {
        DaggerMagicBox.builder().appModule(
            AppModule(
                context
            )
        ).build().poke(this)
    }

    override fun doWork(): Result {
        return runBlocking {
            try {
                val movies = itemRepository.fetchDataFromApi()
                itemRepository.updateAllItems(movies)
                return@runBlocking Result.success()
            } catch (e: HttpException) {
                println(e)
                return@runBlocking Result.failure()
            }
        }
    }

}