package com.vmo.mvvmbaseapp.worker

import androidx.work.*
import java.util.concurrent.TimeUnit


object CustomWorkerFactory {

    private val CONSTRAINTS = Constraints
        .Builder()
        .setRequiredNetworkType(NetworkType.CONNECTED)
        .build()

    fun <T> create(worker: T): WorkRequest? {
        when (worker) {
            SyncItemsWorker::class.java -> {
                return OneTimeWorkRequestBuilder<SyncItemsWorker>()
                    .setConstraints(CONSTRAINTS)
                    .addTag("sync-movie")
                    .build()
            }
            SyncMessageWorker::class.java -> {
                return PeriodicWorkRequestBuilder<SyncMessageWorker>(15, TimeUnit.MINUTES)
                    .setConstraints(CONSTRAINTS)
                    .addTag("sync-message")
                    .build()
            }
            else -> throw Exception("Worker Factory Class Don't Exist")
        }
    }
}