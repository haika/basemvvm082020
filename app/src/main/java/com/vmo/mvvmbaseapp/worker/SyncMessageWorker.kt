package com.vmo.mvvmbaseapp.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.vmo.mvvmbaseapp.network.AppModule
import com.vmo.mvvmbaseapp.helper.DaggerMagicBox
import com.vmo.mvvmbaseapp.helper.NotificationMessage
import com.vmo.mvvmbaseapp.helper.NotificationSender

class SyncMessageWorker(private val context: Context, params: WorkerParameters) : Worker(context, params) {

    init {
        DaggerMagicBox.builder().appModule(
            AppModule(
                context
            )
        ).build().poke(this)
    }

    override fun doWork(): Result {
        NotificationSender.sendNotification(
            context,
            NotificationMessage(1,"Title", "ContentText", "SubText", "ContentInfo")
        )
        return Result.success()
    }

}